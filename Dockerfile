FROM techknowlogick/xgo:latest

# Add arm64 and armhf repos
COPY arm.list /etc/apt/sources.list.d/arm.list

# Add all needed architectures 
RUN dpkg --add-architecture arm64 \
	&& dpkg --add-architecture armhf \
	&& dpkg --add-architecture i386

# Set all current repos to only use amd64 and i386
RUN sed -i 's/deb/deb \[arch=amd64,i386\]/g' /etc/apt/sources.list

RUN apt-get update

# Install Fyne dependencies
RUN apt-get install -y \
	xorg-dev \
	libgl1-mesa-dev \
	libx11-dev \
	libxrandr-dev \
	libxi-dev \
	libxcursor-dev \
	libxinerama-dev \
	libgl1-mesa-dev:i386 \
	libx11-dev:i386 \
	libxrandr-dev:i386 \
	libxi-dev:i386 \
	libxcursor-dev:i386 \
	libxinerama-dev:i386 \
	libgl1-mesa-dev:arm64 \
	libx11-dev:arm64 \
	libxrandr-dev:arm64 \
	libxi-dev:arm64 \
	libxcursor-dev:arm64 \
	libxinerama-dev:arm64 \
	libgl1-mesa-dev:armhf \
	libx11-dev:armhf \
	libxrandr-dev:armhf \
	libxi-dev:armhf \
	libxcursor-dev:armhf \
	libxinerama-dev:armhf

RUN sed -i -e 's|/source|$\{SOURCE_DIR\}|g' -e 's|/build|$\{OUT_DIR\}|g' /build.sh

ENTRYPOINT []
