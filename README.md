# fyne-xgo-docker

This project contains files for building a docker container based on [xgo](https://github.com/techknowlogick/xgo) that contains all of Fyne's dependencies for various architectures.

This allows for building Fyne programs for multiple architectures easily. Currently, the following platforms are supported:

- `linux/amd64`
- `linux/386`
- `linux/arm64`
- `linux/arm` (`GOARM=7`)

The `xgo` build script has been modified so that the `SOURCE_DIR` and `BUILD_DIR` environment variables correspond to where `xgo` look for sources and where it puts its output binaries. This allows for easy use within Continuous Integration engines.

The entrypoint has also been removed to allow running other commands, so run `/build.sh` if you want to start the build.

This container is available on the docker hub as [`arsen6331/fyne-xgo`](https://hub.docker.com/r/arsen6331/fyne-xgo)
